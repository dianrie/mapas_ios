//
//  ViewController.swift
//  mapas
//
//  Created by Diego Angel Fernandez Garcia on 06/06/2019.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit
import Parse
import FirebaseUI


class ViewController: UIViewController{
    
    var mundos : [PFObject] = []
    var avataresImages: [PFObject] = []
    var idUsuario: PFObject? = nil
    
    var activityIndicator :  UIActivityIndicatorView = UIActivityIndicatorView()
    var vista: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    override func viewDidAppear(_ animated: Bool) {
        // Do any additional setup after loading the view.
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        
        let user = Auth.auth().currentUser
        if user != nil {
            print("DATOS USUARIO FIREBASE ID", user?.uid)
            print("DATOS USUARIO FIREBASE NOMBRE",user?.displayName)
            
            //Consultamos si el usuario existe en la base de datos de no existir se crearan sus registros
            let queryUsuarios = PFQuery(className:"Usuarios")
            queryUsuarios.whereKey("idUsuario", equalTo: user!.uid)
            queryUsuarios.findObjectsInBackground { (objects, error) in
                if error == nil {
                    // The find succeeded.
                    // Do something with the found objects
                    if(objects != nil){
                        var crearUsuario = true
                        for object in objects! {
                            if (object["idUsuario"] as? String == user!.uid){
                                    self.idUsuario = objects![0]
                                    crearUsuario = false
                            }
                        }
                        //Creamos el usuario ya que hemos comprobado que no existe
                        if (crearUsuario){
                            let parseObject = PFObject(className:"Usuarios")
                            
                            parseObject["casaLat"] = 0
                            parseObject["casaLng"] = 0
                            let imageData = UIImage(named: "otropersonaje")?.pngData()
                            let imageFile = PFFileObject(name:"image.png", data:imageData!)
                            
                            parseObject["avatar"] = imageFile
                            parseObject["idUsuario"] = user!.uid
                            parseObject["nombre"] = user?.displayName
                            parseObject["lat"] = 0
                            parseObject["nivel"] = 1
                            parseObject["lng"] = 0
                            
                            // Saves the new object.
                            parseObject.saveInBackground {
                                (success: Bool, error: Error?) in
                                if (success) {
                                    // The object has been saved.
                                    //Consulto el nuevo id del usuario creado y creo las mascotas
                                    let queryUsuarios = PFQuery(className:"Usuarios")
                                    queryUsuarios.whereKey("idUsuario", equalTo: user!.uid)
                                    queryUsuarios.findObjectsInBackground { (objects, error) in
                                        if error == nil {
                                            // The find succeeded.
                                            // Do something with the found objects
                                            if(objects != nil){
                                                self.idUsuario = objects![0]
                                                
                                                //-------------------------------
                                                //Creo las mascotas para el usuario recien creado
                                               
                                                for i in 0...3 {
                                                    let parseObject = PFObject(className:"Mascotas")
                                                    
                                                    if (i == 0) {
                                                        parseObject["objectIdUsuario"] = self.idUsuario!.objectId
                                                        parseObject["posicion"] = i
                                                        parseObject["ataque"] = 10
                                                        parseObject["velocidad"] = 50
                                                        parseObject["nombre"] = "Robot"
                                                        parseObject["nivel"] = 1
                                                        parseObject["vida"] = 100
                                                        let imageData = UIImage(named: "zangano")?.pngData()
                                                        let imageFile = PFFileObject(name:"image.png", data:imageData!)
                                                        
                                                        parseObject["icono"] = imageFile
                                                        //parseObject["imagen"] = 1
                                                        
                                                        // Saves the new object.
                                                        parseObject.saveInBackground {
                                                            (success: Bool, error: Error?) in
                                                            if (success) {
                                                                // The object has been saved.
                                                            } else {
                                                                // There was a problem, check error.description
                                                            }
                                                        }
                                                    } else {
                                                        parseObject["objectIdUsuario"] = self.idUsuario!.objectId
                                                        parseObject["posicion"] = i
                                                        parseObject["ataque"] = 1
                                                        parseObject["velocidad"] = 1
                                                        parseObject["nombre"] = ""
                                                        parseObject["nivel"] = 1
                                                        parseObject["vida"] = 1
                                                        //parseObject["icono"] = imageFile
                                                        //parseObject["imagen"] = 1
                                                        
                                                        // Saves the new object.
                                                        parseObject.saveInBackground {
                                                            (success: Bool, error: Error?) in
                                                            if (success) {
                                                                // The object has been saved.
                                                            } else {
                                                                // There was a problem, check error.description
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    //Consultamos las imagenes disponibles en la base de datos para los avatares y abrimos cargamos la lista
                                    let queryAvatares = PFQuery(className:"AvatarUsuarios")
                                    queryAvatares.findObjectsInBackground { (objects, error) in
                                        if error == nil {
                                            // The find succeeded.
                                            // Do something with the found object
                                            for object in objects ?? [] {
                                                //print("\(object["nombre"])")
                                                
                                                self.avataresImages += [object]
                                            }
                                            self.performSegue(withIdentifier: "seleccionarAvatar", sender: self)
                                            
                                        } else {
                                            // Log details of the failure
                                            if let error = error, let user = (error as NSError?)?.userInfo {
                                                print("Error: \(error) \(user)")
                                            }
                                        }
                                    }
                                    
                                } else {
                                    // There was a problem, check error.description
                                    if let error = error, let user = (error as NSError?)?.userInfo {
                                        print("Error: \(error) \(user)")
                                    }
                                }
                            }
                            
                        }else{
                            
                            //Consultamos los mundos en la base de datos y los mandamos a SeleccionarMundosTableViewController
                            if(self.mundos.count == 0){
                                let queryMundos = PFQuery(className:"Mundos")
                                queryMundos.findObjectsInBackground { (objects, error) in
                                    if error == nil {
                                        // The find succeeded.
                                        print("CANTIDAD DE MUNDOS:\(objects?.count ?? 0)")
                                        // Do something with the found objects
                                        for object in objects ?? [] {
                                            print("\(object["nombre"])")
                                            
                                            self.mundos += [object]
                                        }
                                        self.performSegue(withIdentifier: "seleccionarMundo", sender: self)
                                    } else {
                                        // Log details of the failure
                                        if let error = error, let user = (error as NSError?)?.userInfo {
                                            print("Error: \(error) \(user)")
                                        }
                                    }
                                }
                            }else{
                                self.performSegue(withIdentifier: "seleccionarMundo", sender: self)
                            }
                        }
                    }
                } else {
                    // Log details of the failure
                    if let error = error, let user = (error as NSError?)?.userInfo {
                        print("Error: \(error) \(user)")
                    }
                }
            }
            
            
            
            
            
            
        } else {
            //Ejecutamos los metodos para logear al usuario
            print("DATOS USUARIO ID: USUARIO NO REGISTRADO")
            
            let authUI: FUIAuth? = FUIAuth.defaultAuthUI()
            guard authUI != nil else {
                //Log the error
                return
            }
            
            //Delegado
            authUI?.delegate = self
            
            let providers: [FUIAuthProvider] = [
                FUIEmailAuth(),
                FUIGoogleAuth(),
            ]
            
            authUI!.providers = providers
            
            let authViewController = authUI!.authViewController()
            
            //Show it
            present(authViewController, animated: true, completion: nil)
            
        }
        
    }
    //Se abre la vista pasandole los registros de la base de datos
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "seleccionarMundo" {
            let viewController:SeleccionarMundosTableViewController = segue.destination as! SeleccionarMundosTableViewController
            
            viewController.mundos = self.mundos
            viewController.idUsuario = self.idUsuario
            
        }
        
        if segue.identifier == "seleccionarAvatar" {
            let viewController:SeleccionarAvatarCollectionViewController = segue.destination as! SeleccionarAvatarCollectionViewController
            
            viewController.avataresImages = self.avataresImages
            viewController.idUsuario = self.idUsuario
            
            
        }
    }
    
}

extension ViewController: FUIAuthDelegate{
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        //Check if there was an error
        /*
         if error != nil {
         //Log error
         return
         }
         print("DATOS USUARIO ID",authDataResult?.user.uid)
         print("DATOS USUARIO NOMBRE",authDataResult?.user.displayName)
         // authDataResult?.user.uid
         performSegue(withIdentifier: "goHome", sender: self)
         */
    }
    
}

