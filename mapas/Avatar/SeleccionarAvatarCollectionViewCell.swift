//
//  SeleccionarAvatarCollectionViewCell.swift
//  mapas
//
//  Created by Diego Angel Fernandez Garcia on 07/06/2019.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit

class SeleccionarAvatarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imagenAvatar: UIImageView!
    
}
