//
//  Marcador.swift
//  mapas
//
//  Created by Diego Angel Fernandez Garcia on 10/06/2019.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
class Marcador: Equatable {
    static func == (lhs: Marcador, rhs: Marcador) -> Bool {
        return
        lhs.marcador == rhs.marcador &&
        lhs.nombre == rhs.nombre &&
        lhs.nivel == rhs.nivel &&
        lhs.idOtroPersonaje == rhs.idOtroPersonaje &&
        lhs.imagenBitmap == rhs.imagenBitmap &&
        lhs.imagenBitmap2 == rhs.imagenBitmap2 &&
        lhs.mirarIzquierda == rhs.mirarIzquierda &&
        lhs.cambiarPosicionImagen == rhs.cambiarPosicionImagen &&
        lhs.vida == rhs.vida &&
        lhs.ataque == rhs.ataque &&
        lhs.puntuacion == rhs.puntuacion &&
        lhs.velocidad == rhs.velocidad &&
        lhs.seguirPersonaje == rhs.seguirPersonaje &&
        //lhs.irAposicion == rhs.irAposicion &&
        lhs.sizeIcon == rhs.sizeIcon
        
    }
    
    var marcador : GMSMarker
    var nombre = ""
    var nivel = 1
    var idOtroPersonaje = ""
    var imagenBitmap = UIImage(named: "otropersonaje")
    var imagenBitmap2 : UIImage? = nil
    var mirarIzquierda = false
    var cambiarPosicionImagen = true
    var vida = 100
    var ataque = 10
    var distanciaAtaque = 10
    var puntuacion = 0
    var velocidad = 100
    var seguirPersonaje = false
    var irAposicion = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    var sizeIcon = 50
    
    init(marcador: GMSMarker){
        self.marcador = marcador
    }
        
    
}
