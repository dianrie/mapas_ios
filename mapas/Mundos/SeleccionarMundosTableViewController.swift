//
//  SeleccionarMundosTableViewController.swift
//  mapas
//
//  Created by Diego Angel Fernandez Garcia on 07/06/2019.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit
import Parse
import FirebaseUI

class SeleccionarMundosTableViewController: UITableViewController {
    
    var mundos : [PFObject] = []
    var avataresImages: [PFObject] = []
    var idUsuario: PFObject? = nil
    var idMundo : PFObject? = nil
    var buttonLogOut = UIButton()
    var buttonAvatar = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        buttonLogOut = UIButton(frame: CGRect(origin: CGPoint(x: 0.0 , y: self.view.frame.size.height - 70), size: CGSize(width: 100, height: 50)))
        //button.backgroundColor = UIColor.black
        
        buttonLogOut.setTitleColor(UIColor.black, for: .normal)
        buttonLogOut.setTitle("LOGOUT", for: .normal)
        buttonLogOut.addTarget(self, action: #selector(logOut), for: .touchUpInside)
        self.navigationController?.view.addSubview(buttonLogOut)
        //self.view.addSubview(buttonLogOut)
        
        buttonAvatar = UIButton(frame: CGRect(origin: CGPoint(x: self.view.frame.width - 75 , y: self.view.frame.size.height - 70), size: CGSize(width: 50, height: 50)))
        //button.backgroundColor = UIColor.black
        let avatarImage = UIImage(named: "cambioavatar")
        buttonAvatar.setImage(avatarImage, for: .normal)
        buttonAvatar.addTarget(self, action: #selector(seleccionarAvatar), for: .touchUpInside)
        //self.view.addSubview(buttonAvatar)
        self.navigationController?.view.addSubview(buttonAvatar)
        
        /*
         let image = UIImageView(frame: CGRect(origin: CGPoint(x: self.view.frame.width - 100 , y: self.view.frame.size.height - 70), size: CGSize(width: 100, height: 50)))
         image.backgroundColor = UIColor.black
         self.navigationController?.view.addSubview(image)
         */
    }
    override func viewDidAppear(_ animated: Bool) {
        if (buttonAvatar.isHidden){
            buttonAvatar.isHidden = false
            buttonLogOut.isHidden = false
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mundos.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SeleccionarMundosTableViewCell
        // Configure the cell...
        cell.nivel?.text = mundos[indexPath.row]["nombre"] as? String
        let texto1 = mundos[indexPath.row]["usuarioRanking"] as? String ?? "MUNDO"
        let texto2 = mundos[indexPath.row]["puntuacionRanking"] as? Int ?? 1
        cell.ranking?.text = texto1 + ": " + "\(texto2) Puntos "
        //cell.ranking?.text = "\(mundos[indexPath.row]["usuarioRanking"] as! String), \(mundos[indexPath.row]["puntuacionRanking"] as! String)"
        
        //cell.botonRanking.addTarget(self, action: #selector(SeleccionarMundosTableViewController.onClickedButton(_:)), for: .touchUpInside)
        cell.botonRanking.setTitle( String(indexPath.row + 1), for: .normal)
         cell.botonRanking.addTarget(self, action: #selector(onClickedButton), for: .touchUpInside)
        return cell
    }
    
    // Muestro una alerta con los datos del ranking consultados de la base de datos
    @objc func onClickedButton(_ sender: UIButton!) {
        
        print(sender.title(for: .normal)!)
        
    
        let queryMundosUsuarios = PFQuery(className:"MundosUsuarios")
        queryMundosUsuarios.whereKey("objectIdMundo", equalTo: sender.title(for: .normal)!)
        queryMundosUsuarios.whereKey("tipo", equalTo: "usuario")
        queryMundosUsuarios.order(byDescending: "puntuacion")
        queryMundosUsuarios.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                // Do something with the found object
                var mensaje: String = ""
                for (index, object) in (objects?.enumerated())! {
                    //print("\(object["nombre"])")
                    let nombre = object["nombre"] as! String
                    let puntuacionRanking = object["puntuacion"] as! Int
                    print("\(index + 1) \(nombre): \(puntuacionRanking)")
                    mensaje += "\(index + 1) \(nombre): \(puntuacionRanking) \n"
                }
                 self.showAlertView(title: "\(mensaje)", message: nil)
                
            } else {
                // Log details of the failure
                if let error = error, let user = (error as NSError?)?.userInfo {
                    print("Error: \(error) \(user)")
                }
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.idMundo = mundos[indexPath.row]
        self.performSegue(withIdentifier: "mostrarMapa", sender: self)
    }
    
    // MARK: - Botón LogOut
    
    
    @objc func logOut(sender: UIButton!) {
        
        // [START signout]
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
        //self.navigationController?.popToRootViewController(animated: true)
        // [END signout]
    }
    @objc func seleccionarAvatar(sender: UIButton!) {
        
        //Consultamos las imagenes disponibles en la base de datos para los avatares y abrimos cargamos la lista
        if (self.avataresImages.count < 0) {
            self.performSegue(withIdentifier: "seleccionarAvatar", sender: self)
        } else {
        let queryAvatares = PFQuery(className:"AvatarUsuarios")
        queryAvatares.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                // Do something with the found object
                for object in objects ?? [] {
                    //print("\(object["nombre"])")
                    
                    self.avataresImages += [object]
                }
                self.performSegue(withIdentifier: "seleccionarAvatar", sender: self)
                
            } else {
                // Log details of the failure
                if let error = error, let user = (error as NSError?)?.userInfo {
                    print("Error: \(error) \(user)")
                }
            }
        }
    }
       
        //self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
        //self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    //Se abre la vista pasandole los registros de la base de datos
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "mostrarMapa" {
            let viewController:MapaViewController = segue.destination as! MapaViewController
            buttonAvatar.isHidden = true
            buttonLogOut.isHidden = true
            viewController.idMundo = self.idMundo
            viewController.idUsuario = self.idUsuario
        }
        
        if segue.identifier == "seleccionarAvatar" {
            let viewController:SeleccionarAvatarCollectionViewController = segue.destination as! SeleccionarAvatarCollectionViewController
            
            viewController.avataresImages = self.avataresImages
            viewController.idUsuario = self.idUsuario
        }
    
    }
    
    //Moastrar aletar
    func showAlertView(title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            // your code here
            //print("Bye. Lovvy")
            alertController.dismiss(animated: true, completion: nil)
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
