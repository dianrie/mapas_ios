//
//  SeleccionarMundosTableViewCell.swift
//  mapas
//
//  Created by Diego Angel Fernandez Garcia on 06/07/2019.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit

class SeleccionarMundosTableViewCell: UITableViewCell {
    @IBOutlet weak var nivel: UILabel!
    @IBOutlet weak var ranking: UILabel!
    
    @IBOutlet weak var botonRanking: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
