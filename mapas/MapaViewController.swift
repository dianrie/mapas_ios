
//
//  MapaViewController.swift
//  mapas
//
//  Created by Diego Angel Fernandez Garcia on 08/06/2019.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit
import GoogleMaps
import Parse
import MapKit
import AudioToolbox
import AVFoundation


class MapaViewController: UIViewController, GMSMapViewDelegate {
    // MARK: - Variables
    var idMundo : PFObject? = nil
    var idUsuario: PFObject? = nil
    var idMundosUsuarios: PFObject? = nil
    
    var audioPlayer: AVAudioPlayer?
    
    var personaje: Marcador? = nil
    lazy var circleAtaque = GMSCircle()
    lazy var circle = GMSCircle()
    
    var referenciaParaRefresco: CLLocationCoordinate2D? = nil
    var irAposicion: GMSMarker? = nil
    var seguirAMarcador: GMSMarker? = nil
    
    
    var edificiosArray : Array<Marcador> = []
    var enemigosArray : Array<Marcador> = []
    var mascotasArray : Array<Marcador?> = [nil,nil,nil,nil]
    var pokeballArray: Array<Marcador?> = [nil,nil,nil,nil]
    var objetosArray : Array<Marcador> = []
    var respawnArray : Array<Marcador> = []
    var ruinasArray : Array<Marcador> = []
    var energiaArray : Array<Marcador> = []
    var centrosArray:Array<Marcador> = []
    var polilineasArray: Array<GMSPolyline> = []
    var ubicacionesArray: Array<MKMapItem> = []
    
    
    var primeraVez = true
    var findeljuego = false
    var oleada = 1
    var ciclos = 0
    var tamañoMaximoImagen = 100
    var tamañoMinimoImagen = 25
    var distanciaSaltos = 0.00003
    var radioMascotaPerseguirEnemigo = 3000
    var radioMascotaSeguirPersonaje = 50
    var distanciaEnemigoPersiguePersonaje = 300
    var distanciaEdificiosOcultar = 10
    
    //lazy var mapView = GMSMapView()
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var pauseImage: UIButton!
    @IBOutlet weak var atacarImage: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
    
    let puntuacionLabel = UILabel()
    
    // MARK: - Funciones
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // Create a GMSCameraPosition that tells the map to display the
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(idMundo!["latPuntoCentral"] as! Double), longitude: CLLocationDegrees(idMundo!["lngPuntoCentral"] as! Double), zoom: 5.0)
        mapView.camera = camera
        mapView.delegate = self
        //view = mapView
        
        // add icono y label contador de puntos
        let contadorImage = UIImage(named: "atomo")
        let contadorButton = UIButton(type: .custom)
        contadorButton.frame = CGRect(x: 10, y: 125, width: 25, height: 25)
        contadorButton.setImage(contadorImage, for: .normal)
        //button.addTarget(self, action: #selector(self.btnAbsRetClicked(_:)), for:.touchUpInside)
        self.view.addSubview(contadorButton)
        
        
        puntuacionLabel.frame = CGRect(x: 40, y: 125, width: 100, height: 25)
        puntuacionLabel.text = "0"
        self.view.addSubview(puntuacionLabel)
        
        // add button gps
        let gpsImage = UIImage(named: "gps")
        let gpsButton = UIButton(type: .custom)
        gpsButton.frame = CGRect(x: view.frame.width - 45, y: 130, width: 40, height: 40)
        gpsButton.setImage(gpsImage, for: .normal)
        //button.addTarget(self, action: #selector(self.btnAbsRetClicked(_:)), for:.touchUpInside)
        gpsButton.addTarget(self, action: #selector(gpsButtonAccion), for: .touchUpInside)
        self.view.addSubview(gpsButton)
        
        cargarDatosParse()
        
        
    }
    @IBAction func pauseButton(_ sender: Any) {
        if (!findeljuego){
            findeljuego = true
            pauseImage.setImage(UIImage(named: "play"), for: .normal)
        }else{
            findeljuego = false
            pauseImage.setImage(UIImage(named: "pausa"), for: .normal)
            self.moverPersonajes()
        }
    }
    @IBAction func mapButton(_ sender: Any) {
        if (mapView.mapType == .normal ){
            mapView.mapType = .hybrid
        } else {
            mapView.mapType = .normal
        }
    }
    @IBAction func atacarButton(_ sender: Any) {
        self.showAlertView(title: "Ataque: \(personaje!.ataque)", message: nil)
    }
    @IBAction func pokeballButton(_ sender: Any) {
        pokeball(index: 0)
        pokeball(index: 0)
    }
    @objc func gpsButtonAccion(sender: UIButton!) {
        mapView.camera = GMSCameraPosition.camera(withTarget: (personaje?.marcador.position)!, zoom: 15.0)
    }
    
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        irAposicion?.map = nil
        irAposicion = nil
        seguirAMarcador = marker
        
        // irAposicion(coordenadasInicio: marker.position)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        seguirAMarcador = nil
        irAposicion(coordenadasInicio: coordinate)
    }
    
    func cargarDatosParse() {
        //Se cargan los datos de la base de datos
        let coordenadesInicio = CLLocationCoordinate2D(latitude: CLLocationDegrees(idMundo!["latPuntoCentral"] as! Double), longitude: CLLocationDegrees(idMundo!["lngPuntoCentral"] as! Double))
        iniciarJuego(coordenadasInicio: coordenadesInicio)
        
        //Creo  Un array con los ciudades cercanas consultando la API de sitios cercanos
        /*
         val getPlaces = GetPlaces(
         this@RolActivity, referenciaParaRefresco!!
         )
         getPlaces.execute()
         */
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = "ayuntamiento"
        searchRequest.region = MKCoordinateRegion(center: coordenadesInicio, latitudinalMeters: 10000, longitudinalMeters: 10000)
        let search = MKLocalSearch(request: searchRequest)
        
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                
                return
            }
            
            for item in response.mapItems {
                self.ubicacionesArray.append(item)
                print(item.name ?? "No phone number.")
            }
        }
        
        //Consultamos la tabla MundosUsuarios
        let queryMundosUsuarios = PFQuery(className:"MundosUsuarios")
        queryMundosUsuarios.whereKey("objectIdMundo", contains: self.idMundo?["nivel"] as? String)
        queryMundosUsuarios.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                // Do something with the found object
                for object in objects ?? [] {
                    
                    //Si el registro es de tipo usuario y coincide con el usuario actual
                    if object["objectIdUsuario"] as? String == self.idUsuario?.objectId {
                        self.idMundosUsuarios = object
                        
                        self.personaje?.distanciaAtaque = object["distanciaAtaque"] as! Int
                        self.personaje?.nombre =  object["nombre"] as! String
                        self.personaje?.nivel =  object["nivel"] as! Int
                        self.personaje?.ataque =  object["ataque"] as! Int
                        self.personaje?.vida =  object["vida"] as! Int
                        self.personaje?.velocidad =  object["velocidad"] as! Int
                        self.personaje?.puntuacion =  object["puntuacion"] as! Int
                        self.circleAtaque.radius = CLLocationDistance(self.personaje!.ataque)
                        
                        //Creo las mascotas consultando en la base de datos y añadiendolas en el array mascotas
                        let queryMascotas = PFQuery(className:"Mascotas")
                        queryMascotas.whereKey("objectIdUsuario", equalTo: self.idUsuario?.objectId as Any)
                        queryMascotas.findObjectsInBackground { (objects, error) in
                            if error == nil {
                                // The find succeeded.
                                // Do something with the found object
                                for object in objects ?? [] {
                                    //print("\(object["nombre"])")
                                    if object["nombre"] as! String != "" {
                                        
                                        let imagenObject = object["icono"] as! PFFileObject
                                        do {
                                            let imageData = try imagenObject.getData()
                                            
                                            let mascota = self.crearMarcador(coordenadasInicio: self.referenciaParaRefresco!, nombre: object["nombre"] as! String, imagen: UIImage(data: imageData)!)
                                            
                                            mascota.imagenBitmap = UIImage(data: imageData)
                                            mascota.marcador.zIndex = 2
                                            mascota.velocidad = object["velocidad"] as! Int
                                            mascota.vida = object["vida"] as! Int
                                            mascota.ataque = object["ataque"] as! Int
                                            mascota.seguirPersonaje = true
                                            mascota.nivel = object["nivel"] as! Int
                                            mascota.seguirPersonaje = true
                                            
                                            self.pokeballArray.insert(mascota, at: object["posicion"] as! Int)
                                            self.pokeball(index: object["posicion"] as! Int)
                                            
                                        } catch let signOutError as NSError {
                                            print ("Error signing out: %@", signOutError)
                                        }
                                    }
                                }
                            } else {
                                // Log details of the failure
                                if let error = error, let user = (error as NSError?)?.userInfo {
                                    print("Error: \(error) \(user)")
                                }
                            }
                        }
                        
                    }
                    
                    //Si el registro es de tipo enemigo
                    if object["tipo"] as? String == "enemigo" {
                        let respawn = self.crearMarcador(coordenadasInicio: self.posicionAleatoriaRelativaEnemigos(coordenadasInicio: self.referenciaParaRefresco!), nombre: "Portal", imagen: UIImage(named: "portal")!)
                        
                        let imagenObject = object["icono"] as! PFFileObject
                        do {
                            let imageData = try imagenObject.getData()
                            
                            respawn.imagenBitmap = UIImage(data: imageData)
                            
                            respawn.distanciaAtaque = object["distanciaAtaque"] as! Int
                            respawn.nombre =  object["nombre"] as! String
                            respawn.nivel =  object["nivel"] as! Int
                            respawn.ataque =  object["ataque"] as! Int
                            respawn.vida =  object["vida"] as! Int
                            respawn.velocidad =  object["velocidad"] as! Int
                            respawn.idOtroPersonaje =  object.objectId!
                            
                            self.respawnArray.append(respawn)
                            
                            if (3 > self.respawnArray.count) {
                                self.ejecutarRespawn(respawn: respawn)
                                self.seguirAMarcador = self.masCercanoDelArray(coordenadasInicio: self.personaje!.marcador.position, array: self.enemigosArray)?.marcador
                            } else {
                                respawn.marcador.map = nil
                            }
                            
                        } catch let signOutError as NSError {
                            print ("Error signing out: %@", signOutError)
                        }
                    }
                    //Si el registro es de tipo edificio
                    if object["tipo"] as? String == "edificio" {
                        let edificio = self.crearMarcador(coordenadasInicio: self.posicionAleatoriaRelativaEdificios(coordenadasInicio: self.referenciaParaRefresco!), nombre: object["nombre"] as? String ?? "", imagen: UIImage(named: "edificio")!)
                        
                        let imagenObject = object["icono"] as! PFFileObject
                        do {
                            let imageData = try imagenObject.getData()
                            
                            edificio.imagenBitmap = UIImage(data: imageData)
                            edificio.marcador.icon = UIImage(data: imageData)?.withSize(width: 50, height: 50)
                            edificio.distanciaAtaque = object["distanciaAtaque"] as! Int
                            edificio.nombre =  object["nombre"] as! String
                            edificio.nivel =  object["nivel"] as! Int
                            edificio.ataque =  object["ataque"] as! Int
                            edificio.vida =  object["vida"] as! Int
                            edificio.velocidad =  object["velocidad"] as! Int
                            edificio.idOtroPersonaje =  object.objectId!
                            
                            self.edificiosArray.append(edificio)
                            
                        } catch let signOutError as NSError {
                            print ("Error signing out: %@", signOutError)
                        }
                    }
                    
                    //Si el registro es de tipo objeto
                    if object["tipo"] as? String == "objeto" {
                        let objeto = self.crearMarcador(coordenadasInicio: self.posicionAleatoriaRelativaEdificios(coordenadasInicio: self.referenciaParaRefresco!), nombre: object["nombre"] as? String ?? "", imagen: UIImage(named: "paquete")!)
                        
                        let imagenObject = object["icono"] as! PFFileObject
                        do {
                            let imageData = try imagenObject.getData()
                            
                            objeto.imagenBitmap = UIImage(data: imageData)
                            objeto.distanciaAtaque = object["distanciaAtaque"] as! Int
                            objeto.nombre =  object["nombre"] as! String
                            objeto.nivel =  object["nivel"] as! Int
                            objeto.ataque =  object["ataque"] as! Int
                            objeto.vida =  object["vida"] as! Int
                            objeto.velocidad =  object["velocidad"] as! Int
                            objeto.idOtroPersonaje =  object.objectId!
                            objeto.marcador.zIndex = 4000
                            objeto.irAposicion = CLLocationCoordinate2D(latitude: objeto.marcador.position.latitude - 0.01, longitude: objeto.marcador.position.longitude + 0.01)
                            
                            self.objetosArray.append(objeto)
                            
                        } catch let signOutError as NSError {
                            print ("Error signing out: %@", signOutError)
                        }
                    }
                }
                
            } else {
                // Log details of the failure
                if let error = error, let user = (error as NSError?)?.userInfo {
                    print("Error: \(error) \(user)")
                }
            }
        }
    }
    
    func iniciarJuego(coordenadasInicio: CLLocationCoordinate2D){
        
        referenciaParaRefresco = coordenadasInicio
        
        let imagenObject = idUsuario!["avatar"] as! PFFileObject
        do {
            let imageData = try imagenObject.getData()
            personaje = crearMarcador(coordenadasInicio: coordenadasInicio, nombre: "personaje", imagen: UIImage(data: imageData)!)
            personaje?.imagenBitmap = UIImage(data: imageData)!
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        personaje?.marcador.zIndex = 2
        circleAtaque = GMSCircle(position: referenciaParaRefresco!, radius: 100)
        circleAtaque.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
        circleAtaque.strokeColor = UIColor(red: 0, green: 0, blue: 255, alpha: 0.50)
        circleAtaque.map = mapView
        
        
        mapView.camera = GMSCameraPosition.camera(withTarget: coordenadasInicio, zoom: 13.0)
        // mapView.animate(toZoom: 20)
        circle = GMSCircle(position: referenciaParaRefresco!, radius: 100)
        circle.fillColor = UIColor(red: 0, green: 0, blue: 255, alpha: 0.1)
        circle.strokeColor = UIColor(red: 0, green: 0, blue: 255, alpha: 0.5)
        circle.map = mapView
        
        //Creo la estructura central
        let casa = crearMarcador(coordenadasInicio: coordenadasInicio, nombre: "Centro", imagen: UIImage(named: "casa")!)
        casa.marcador.zIndex = 2
        
        edificiosArray.append(casa)
        
        //Inicio la tarea asingrona MoverPersonajes
        DispatchQueue.main.async {self.moverPersonajes()}
        
        
    }
    
    func pokeball(index : Int) {
        //funcion que controla si cuando la mascota sale del centro
        if (mascotasArray[index] != nil || pokeballArray[index] != nil) {
            //sale de la pokeball
            if (mascotasArray[index] == nil) {
                if (pokeballArray[index]!.vida <= 0) {
                    //Toast.makeText(applicationContext, "Drone reparado ",Toast.LENGTH_SHORT).show()
                }
                mascotasArray[index] = pokeballArray[index]
                mascotasArray[index]!.marcador.zIndex = 1
                
                
                mascotasArray[index]!.marcador.position = self.referenciaParaRefresco!
                mascotasArray[index]!.seguirPersonaje = true
                
                pokeballArray[index] = nil
                
                ///Sonido
                playSound(fileName: "robot")
                //SystemSoundID.playFileNamed(fileName: "robot", withExtenstion: "mp3")
            } else {
                //Entra en la pokeball
                
                mascotasArray[index]!.marcador.map = nil
                pokeballArray[index] = mascotasArray[index]
                mascotasArray[index] = nil
            }
        }
        
    }
    
    func irAposicion(coordenadasInicio: CLLocationCoordinate2D ) {
        
        irAposicion?.map = nil
        irAposicion = nil
        
        let marker = GMSMarker(position: coordenadasInicio)
        
        marker.icon = UIImage(named: "iraposicion")?.withSize(width: 20, height: 20)
        marker.title = "Mover aqui"
        marker.map = mapView
        irAposicion = marker
        
        
    }
    
    func masCercanoDelArray(coordenadasInicio: CLLocationCoordinate2D, array: Array<Marcador?>) -> Marcador? {
        var posicionElementoMasCercano: Marcador? = nil
        
        // Evaluamos que elemento del array esta mas cerca
        if (array.count != 0) {
            var distancia = 30000
            
            for elemento in array {
                if (elemento?.marcador.map != nil) {
                    
                    if (distanciaA(
                        posicion1: coordenadasInicio,
                        posicion2: elemento!.marcador.position
                        ) < distancia && elemento?.marcador.map != nil
                        ) {
                        posicionElementoMasCercano = elemento
                        distancia = distanciaA(posicion1: coordenadasInicio, posicion2: elemento!.marcador.position)
                    }
                }
            }
        }
        return posicionElementoMasCercano
    }
    
    func distanciaA(posicion1 : CLLocationCoordinate2D, posicion2: CLLocationCoordinate2D) -> Int {
        let location1 = CLLocation(latitude: CLLocationDegrees(posicion1.latitude), longitude: CLLocationDegrees(posicion1.longitude))
        let location2 = CLLocation(latitude: CLLocationDegrees(posicion2.latitude), longitude: CLLocationDegrees(posicion2.longitude))
        
        return Int(location1.distance(from: location2))
    }
    
    func ejecutarRespawn(respawn enemigo1 : Marcador?) {
        //funcion que genera un enemigo en el respawn del que salio
        
        var respawn: Marcador? = nil
        
        if (enemigo1 != nil) {
            var index = self.respawnArray.firstIndex(where: {$0 === enemigo1})
            
            for respawn1 in respawnArray {
                if (enemigo1!.idOtroPersonaje == respawn1.idOtroPersonaje) {
                    respawn1.marcador.map = nil
                    
                }
            }
            for enemigo in enemigosArray {
                if (enemigo.idOtroPersonaje == enemigo1!.idOtroPersonaje) {
                    index = Int.random(in: 0...respawnArray.count - 1 )
                }
            }
            respawn = respawnArray[index!]
            respawn!.marcador.map = self.mapView
            //respawn.ataque += enemigo1.puntuacion
            respawn!.vida += enemigo1!.puntuacion
            respawn!.puntuacion += respawn!.vida
            
            respawn!.marcador.position = posicionAleatoriaRelativaEnemigos(coordenadasInicio: referenciaParaRefresco!)
            //respawn.distanciaAtaque += enemigo1.puntuacion
            // respawn.velocidad += enemigo1.puntuacion
            if (respawn!.velocidad >= 100) {
                respawn!.velocidad = 100
            }
            if (respawn!.puntuacion >= 100) {
                respawn!.puntuacion = 100
            }
            
            if (respawn != nil) {
                let enemigo = self.crearMarcador(coordenadasInicio: respawn!.marcador.position, nombre: respawn!.nombre, imagen: respawn!.imagenBitmap!)
                //Se controla que el enemigo na sea mas grande de 200 x 200
                //var imagen = respawn.vida
                
                enemigo.sizeIcon = Int.random(in: 0...self.tamañoMaximoImagen)
                
                
                if (enemigo.sizeIcon < tamañoMinimoImagen) {
                    enemigo.sizeIcon = tamañoMinimoImagen
                }
                
                enemigo.marcador.icon = respawn!.imagenBitmap!.withSize(width: CGFloat(enemigo.sizeIcon), height: CGFloat(enemigo.sizeIcon))
                enemigo.imagenBitmap = respawn!.imagenBitmap
                
                enemigo.idOtroPersonaje = respawn!.idOtroPersonaje
                enemigo.velocidad = respawn!.velocidad
                enemigo.ataque = respawn!.ataque
                enemigo.vida = respawn!.vida
                enemigo.nivel = respawn!.nivel
                enemigo.puntuacion = respawn!.puntuacion
                enemigo.marcador.zIndex = 3000
                //enemigo.nombre = respawn.nombre
                
                //lo añado al array para que se comporte como un enemigo
                enemigosArray.append(enemigo)
            }
            
        } else {
            for respawn in respawnArray {
                ejecutarRespawn(respawn: respawn)
            }
            
        }
        
    }
    
    
    func posicionAleatoriaRelativaEnemigos(coordenadasInicio: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        //Genero una posicion aleatoria tomando como referencia las coordenadas pasadas fuera del rango de 2km
        var posicionFinal: CLLocationCoordinate2D
        let lat1 = Double.random(in: 0.0...0.019)
        let lng1 = Double.random(in: 0.018...0.020)
        let lat2 = Double.random(in: 0.018...0.020)
        let lng2 = Double.random(in: 0.0...0.019)
        
        if (Bool.random()) {
            if (Bool.random()) {
                if (Bool.random()) {
                    posicionFinal = CLLocationCoordinate2D(latitude: coordenadasInicio.latitude + lat1, longitude: coordenadasInicio.longitude + lng1)
                } else {
                    posicionFinal = CLLocationCoordinate2D(latitude: coordenadasInicio.latitude - lat1, longitude: coordenadasInicio.longitude - lng1)
                }
            } else {
                if (Bool.random()) {
                    posicionFinal =  CLLocationCoordinate2D(latitude: coordenadasInicio.latitude + lat1, longitude: coordenadasInicio.longitude - lng1)
                } else {
                    posicionFinal = CLLocationCoordinate2D(latitude: coordenadasInicio.latitude - lat1, longitude: coordenadasInicio.longitude + lng1)
                }
            }
        } else {
            if (Bool.random()) {
                if (Bool.random()) {
                    posicionFinal =  CLLocationCoordinate2D(latitude: coordenadasInicio.latitude + lat2, longitude: coordenadasInicio.longitude + lng2)
                } else {
                    posicionFinal = CLLocationCoordinate2D(latitude: coordenadasInicio.latitude - lat2, longitude: coordenadasInicio.longitude - lng2)
                }
            } else {
                if (Bool.random()) {
                    posicionFinal =  CLLocationCoordinate2D(latitude: coordenadasInicio.latitude + lat2, longitude: coordenadasInicio.longitude - lng2)
                } else {
                    posicionFinal =  CLLocationCoordinate2D(latitude: coordenadasInicio.latitude - lat2, longitude: coordenadasInicio.longitude + lng2)
                }
            }
        }
        return posicionFinal
        
    }
    
    func posicionAleatoriaRelativaEdificios(coordenadasInicio: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        //Genero una posicion aleatoria tomando como referencia las coordenadas pasadas fuera del rango de 2km
        var posicionFinal: CLLocationCoordinate2D
        let ramdom = Double.random(in: 0.0001...0.014)
        
        if (Bool.random()) {
            if (Bool.random()) {
                if (Bool.random()) {
                    posicionFinal = CLLocationCoordinate2D(latitude: coordenadasInicio.latitude + ramdom, longitude: coordenadasInicio.longitude + ramdom)
                } else {
                    posicionFinal = CLLocationCoordinate2D(latitude: coordenadasInicio.latitude - ramdom, longitude: coordenadasInicio.longitude - ramdom)
                }
            } else {
                if (Bool.random()) {
                    posicionFinal =  CLLocationCoordinate2D(latitude: coordenadasInicio.latitude + ramdom, longitude: coordenadasInicio.longitude - ramdom)
                } else {
                    posicionFinal = CLLocationCoordinate2D(latitude: coordenadasInicio.latitude - ramdom, longitude: coordenadasInicio.longitude + ramdom)
                }
            }
        } else {
            if (Bool.random()) {
                if (Bool.random()) {
                    posicionFinal =  CLLocationCoordinate2D(latitude: coordenadasInicio.latitude + ramdom, longitude: coordenadasInicio.longitude + ramdom)
                } else {
                    posicionFinal = CLLocationCoordinate2D(latitude: coordenadasInicio.latitude - ramdom, longitude: coordenadasInicio.longitude - ramdom)
                }
            } else {
                if (Bool.random()) {
                    posicionFinal =  CLLocationCoordinate2D(latitude: coordenadasInicio.latitude + ramdom, longitude: coordenadasInicio.longitude - ramdom)
                } else {
                    posicionFinal =  CLLocationCoordinate2D(latitude: coordenadasInicio.latitude - ramdom, longitude: coordenadasInicio.longitude + ramdom)
                }
            }
        }
        return posicionFinal
        
    }
    func moverPersonaje(coordenadasInicio: CLLocationCoordinate2D, posicionFinal: CLLocationCoordinate2D, velocidad: Int) -> CLLocationCoordinate2D {
        if distanciaA(posicion1:coordenadasInicio, posicion2: posicionFinal) < 10 {
            irAposicion?.map = nil
            irAposicion = nil
        }
        circleAtaque.radius = CLLocationDistance(personaje!.distanciaAtaque)
        
        // Evaluamos si esta cerca de un edificio
        if (edificiosArray.count != 0) {
            
            for edificio in edificiosArray {
                if (edificio.marcador.map != nil) {
                    if (distanciaA(posicion1: coordenadasInicio, posicion2: edificio.marcador.position) < distanciaEdificiosOcultar) {
                        personaje!.vida = 100
                        progressBar.progress = Float(personaje!.vida) / 100.0
                        seguirAMarcador = masCercanoDelArray(coordenadasInicio: personaje!.marcador.position, array: enemigosArray)?.marcador
                        //val imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.ambulancia)
                        if (personaje!.imagenBitmap2 != nil) {
                            personaje!.marcador.icon = personaje!.imagenBitmap2?.withSize(width: 50, height: 50)
                            personaje!.imagenBitmap = personaje!.imagenBitmap2
                            personaje!.imagenBitmap2 = nil
                        }
                        //Sonido
                        playSound(fileName: "telemetria")
                        //SystemSoundID.playFileNamed(fileName: "telemetria", withExtenstion: "mp3")
                    }
                }
            }
        }
        
        // Evaluamos si esta cerca de un objeto
        if (objetosArray.count != 0) {
            
            for objeto in objetosArray {
                if (objeto.marcador.map != nil) {
                    if (distanciaA(posicion1: posicionFinal, posicion2: objeto.marcador.position) < 300) {
                        seguirAMarcador = masCercanoDelArray(coordenadasInicio: personaje!.marcador.position, array: enemigosArray)?.marcador
                        atacarImage.setImage(objeto.imagenBitmap, for: .normal)
                        progressBar.progress = Float(personaje!.vida) / 100
                        
                        personaje!.vida = objeto.vida
                        personaje!.ataque += objeto.ataque
                        personaje!.distanciaAtaque += objeto.distanciaAtaque
                        personaje!.velocidad = objeto.velocidad
                        circleAtaque.radius = CLLocationDistance(personaje!.distanciaAtaque)
                        
                        if (personaje?.imagenBitmap2 != nil){
                            personaje?.imagenBitmap = personaje?.imagenBitmap2
                        }
                        
                        //objeto.vida = 0
                        objeto.marcador.map = nil
                        //Sonido
                        playSound(fileName: "objeto")
                        //SystemSoundID.playFileNamed(fileName: "objeto", withExtenstion: "mp3")
                    }
                }
            }
            
        }
        let coordenadas = coordenadasDestino(marcador: personaje!, posicionFinal: posicionFinal)
        circleAtaque.position = coordenadas
        return coordenadas
    }
    
    func moverMascota(mascota: Marcador, posicionFinal: CLLocationCoordinate2D, velocidad: Int) -> CLLocationCoordinate2D {
        if (!moverPorCiclo(velocidad: velocidad)) {
            return mascota.marcador.position
        }
        mascota.marcador.map = mapView
        let posicionInicial = mascota.marcador.position
        
        //Evaluamos si esta cerca de un enemigo
        if (enemigosArray.count != 0) {
            let enemigoCercano = masCercanoDelArray(coordenadasInicio: mascota.marcador.position, array: enemigosArray)
            if (enemigoCercano != nil) {
                if (distanciaA(posicion1: mascota.marcador.position, posicion2: enemigoCercano!.marcador.position) < mascota.distanciaAtaque) {
                    
                    enemigoCercano!.vida -= mascota.ataque
                    
                    //Toast.makeText(this, "enemigo vida: -10", Toast.LENGTH_LONG).show()
                    //enemigoCercano.marcador.snippet = "vida: ${enemigoCercano.vida}%"
                    
                    //Cambiamos el tamaño de la imagen del enemigo segun se reduzga su vida
                    enemigoCercano!.sizeIcon -= 10
                    if (enemigoCercano!.sizeIcon < tamañoMinimoImagen) {
                        enemigoCercano!.sizeIcon = tamañoMinimoImagen
                    }
                    
                    enemigoCercano!.marcador.icon = enemigoCercano!.imagenBitmap?.withSize(width: CGFloat(enemigoCercano!.sizeIcon), height: CGFloat(enemigoCercano!.sizeIcon))
                    
                    //Si le quitamos vida al enemigo se aleja de la mascota
                    enemigoCercano!.marcador.position = CLLocationCoordinate2D(latitude: posicionInicial.latitude + 0.0009, longitude: posicionInicial.longitude + 0.0009)
                    //Sonido
                    playSound(fileName: "laser")
                    //SystemSoundID.playFileNamed(fileName: "laser", withExtenstion: "mp3")
                    
                }
                //Si la vida del enemigo es meror o igual a 0 lo elimino y creo energia
                if (enemigoCercano!.vida <= 0) {
                    let energia = crearMarcador(coordenadasInicio: enemigoCercano!.marcador.position, nombre: "Energia", imagen: (UIImage(named: "atomo")?.withSize(width: 25, height: 25))!)
                    energia.sizeIcon = 25
                    energia.imagenBitmap = UIImage(named: "atomo")
                    energia.puntuacion = enemigoCercano!.puntuacion
                    energiaArray.append(energia)
                    personaje!.puntuacion += enemigoCercano!.puntuacion
                    puntuacionLabel.text = String(personaje!.puntuacion)
                    ejecutarRespawn(respawn: enemigoCercano!)
                    enemigoCercano!.marcador.map = nil
                    enemigosArray.removeObjet(object: enemigoCercano!)
                    //Sonido
                    playSound(fileName: "muertemostruo")
                    //SystemSoundID.playFileNamed(fileName: "muertemonstruo", withExtenstion: "mp3")
                }
            }
        }
        // Evaluamos si esta cerca de un edificio si es un hospital se cura
        if (edificiosArray.count != 0) {
            for edificio in edificiosArray {
                if (edificio.marcador.map != nil) {
                    
                    if (distanciaA(posicion1: posicionInicial, posicion2: edificio.marcador.position) < distanciaEdificiosOcultar) {
                        mascota.marcador.map = nil
                        
                        if (mascota.vida < 100) {
                            mascota.vida += 100
                            mascota.sizeIcon += 50
                            if (mascota.sizeIcon > tamañoMaximoImagen) {
                                mascota.sizeIcon = tamañoMaximoImagen
                            }
                            //Sonido
                            playSound(fileName: "telemetria")
                            //SystemSoundID.playFileNamed(fileName: "telemetria", withExtenstion: "mp3")
                        }
                        
                    }
                }
            }
        }
        
        
        return coordenadasDestino(marcador: mascota, posicionFinal: posicionFinal)
    }
    
    func moverEnemigo(enemigo: Marcador, posicionFinal: CLLocationCoordinate2D, velocidad: Int) -> CLLocationCoordinate2D {
        if (!moverPorCiclo(velocidad: velocidad)) {
            return enemigo.marcador.position
        }
        
        enemigo.marcador.map = mapView
        let posicionInicial = enemigo.marcador.position
        
        
        //Evaluo si esta cerca del personaje para aplicarle el daño lo hago en la siguiente
        if (distanciaA(posicion1: posicionInicial,posicion2: personaje!.marcador.position) < enemigo.distanciaAtaque && personaje!.marcador.map != nil) {
            if (personaje!.vida <= 0) {
                // estas muerto
            } else {
                personaje?.vida -= enemigo.ataque
                progressBar.progress = Float(personaje!.vida) / 100.0
                
            }
            //Si le quitamos vida al personaje se alaja del enemigo
            // personaje!!.marcador.position = LatLng(posicionInicial.latitude + 0.0001, posicionInicial.longitude + 0.0001)
            
        }
        
        if (distanciaA(posicion1: posicionInicial,posicion2: personaje!.marcador.position) < personaje!.distanciaAtaque && personaje!.marcador.map != nil) {
            
            //Evaluo que daño se le aplica al enemigo por enfrentarse al personaje
            if (circleAtaque.map != nil) {
                if (enemigo.vida > 0) {
                    enemigo.vida -= personaje!.ataque
                    
                    //Cambiamos el tamaño de la imagen del enemigo segun se reduzga su vida
                    enemigo.sizeIcon -= 10
                    if (enemigo.sizeIcon < tamañoMinimoImagen) {
                        enemigo.sizeIcon = tamañoMinimoImagen
                    }
                    enemigo.marcador.icon = enemigo.imagenBitmap?.withSize(width: CGFloat(enemigo.sizeIcon), height: CGFloat(enemigo.sizeIcon))
                    
                    if (enemigo.vida > 0) {
                        seguirAMarcador = enemigo.marcador
                    } else {
                        seguirAMarcador = nil
                        irAposicion?.map = nil
                        irAposicion = nil
                        enemigo.marcador.map = nil
                    }
                    
                    
                    if (personaje!.distanciaAtaque > 10) {
                        personaje!.distanciaAtaque -= enemigo.distanciaAtaque
                        
                        if (personaje!.distanciaAtaque < 10) {
                            personaje!.distanciaAtaque = 10
                            circleAtaque.radius = CLLocationDistance(personaje!.distanciaAtaque)
                        }
                        
                    } else {
                        //Cuando la distancia de ataque es menor de 10 vuelven a salir los objetos en el mapa
                        for objeto in objetosArray {
                            if (objeto.marcador.map == nil) {
                                objeto.marcador.position = posicionAleatoriaRelativaEdificios(coordenadasInicio: referenciaParaRefresco!)
                                objeto.marcador.map = mapView
                                
                                
                                objeto.marcador.icon = UIImage(named: "paquete")?.withSize(width: 50, height: 50)
                                objeto.irAposicion = CLLocationCoordinate2D(latitude: objeto.marcador.position.latitude - 0.01, longitude: objeto.marcador.position.longitude + 0.01)
                                
                            }
                        }
                    }
                }
                //Si le quitamos vida al enemigo se alaja del personaje controlamos si es a la derechea o a la izquierda
                if (personaje!.mirarIzquierda) {
                    enemigo.marcador.position = CLLocationCoordinate2D(latitude: posicionInicial.latitude, longitude: posicionInicial.longitude - 0.001)
                    personaje!.marcador.position = CLLocationCoordinate2D(latitude: personaje!.marcador.position.latitude,longitude: personaje!.marcador.position.longitude + 0.001)
                    
                } else {
                    enemigo.marcador.position = CLLocationCoordinate2D(latitude: posicionInicial.latitude, longitude: posicionInicial.longitude + 0.001)
                    personaje!.marcador.position = CLLocationCoordinate2D(
                        latitude: personaje!.marcador.position.latitude,
                        longitude: personaje!.marcador.position.longitude - 0.001
                    )
                }
                if (enemigo.marcador.position.latitude > personaje!.marcador.position.latitude) {
                    enemigo.marcador.position =
                        CLLocationCoordinate2D(latitude: posicionInicial.latitude + 0.0001, longitude: enemigo.marcador.position.longitude)
                    personaje!.marcador.position = CLLocationCoordinate2D(
                        latitude: personaje!.marcador.position.latitude - 0.0001,
                        longitude: personaje!.marcador.position.longitude
                    )
                } else {
                    enemigo.marcador.position =
                        CLLocationCoordinate2D(latitude: posicionInicial.latitude - 0.0001, longitude: enemigo.marcador.position.longitude)
                    personaje!.marcador.position = CLLocationCoordinate2D(
                        latitude: personaje!.marcador.position.latitude + 0.0001,
                        longitude: personaje!.marcador.position.longitude
                    )
                }
                circleAtaque.position = personaje!.marcador.position
                //Sonido
                playSound(fileName: "golpe")
                //SystemSoundID.playFileNamed(fileName: "golpe", withExtenstion: "mp3")
                
            }
        }
        
        //Evaluamos si esta cerca del circulo de la torre central
        if (distanciaA(posicion1: enemigo.marcador.position, posicion2: referenciaParaRefresco!) < Int(circle.radius)) {
            //Si le quitamos vida al enemigo se alaja del personaje controlamos si es a la derechea o a la izquierda
            if (enemigo.mirarIzquierda) {
                enemigo.marcador.position = CLLocationCoordinate2D(latitude: posicionInicial.latitude, longitude: posicionInicial.longitude + 0.001)
                
            } else {
                enemigo.marcador.position = CLLocationCoordinate2D(latitude: posicionInicial.latitude, longitude: posicionInicial.longitude - 0.001)
            }
            //Si le quitamos vida al enemigo se alaja del personaje controlamos si es a la derechea o a la izquierda
            if (enemigo.marcador.position.latitude > referenciaParaRefresco!.latitude) {
                enemigo.marcador.position =
                    CLLocationCoordinate2D(latitude: posicionInicial.latitude + 0.001, longitude: enemigo.marcador.position.longitude)
                
            } else {
                enemigo.marcador.position =
                    CLLocationCoordinate2D(latitude: posicionInicial.latitude - 0.001, longitude: enemigo.marcador.position.longitude)
                
            }
            if (circle.radius < 10) {
                circle.radius = 0.0
            } else {
                if (enemigo.ataque >= Int(circle.radius)) {
                    circle.radius = 0.0
                } else {
                    circle.radius -= CLLocationDistance(enemigo.ataque)
                }
                
            }
        }
        
        // Evaluamos si esta cerca de un edificio y alejado mas de distanciaEdificiosOcultar
        if (edificiosArray.count != 0) {
            for edificio in edificiosArray {
                if ( edificio.marcador.map != nil) {
                    if (distanciaA(posicion1: posicionInicial, posicion2: edificio.marcador.position) < distanciaEdificiosOcultar) {
                        if (edificio.vida > 0) {
                            edificio.vida -= personaje!.ataque
                            //Sonido cuando un enemigo golpea un edificio
                            playSound(fileName: "golpe")
                            //SystemSoundID.playFileNamed(fileName: "golpe", withExtenstion: "mp3")
                        }
                        enemigo.marcador.position = CLLocationCoordinate2D(latitude: posicionInicial.latitude - 0.001, longitude: posicionInicial.longitude - 0.001)
                    }
                }
            }
        }
        
        //Comprobamos si hay alguna mascota cerca y aplicamos el daño
        if (mascotasArray.count != 0) {
            let mascotaCercana = masCercanoDelArray(coordenadasInicio: enemigo.marcador.position, array: mascotasArray)
            
            if (mascotaCercana != nil) {
                if (distanciaA(posicion1: enemigo.marcador.position, posicion2: mascotaCercana!.marcador.position) < enemigo.distanciaAtaque) {
                    if (mascotaCercana!.vida <= 0) {
                        //mascotaCercana.seguirPersonaje = false
                        mascotaCercana!.ataque += 10
                        var index = self.mascotasArray.firstIndex(where: {$0 === mascotaCercana!})
                        
                        switch (index) {
                        case 0 : do {
                            self.pokeball(index: 0)
                            self.pokeball(index: 0)
                            }
                            
                        case  1 : do {
                            self.pokeball(index: 1)
                            self.pokeball(index: 1)
                            
                            }
                        case  2 : do {
                            self.pokeball(index: 2)
                            self.pokeball(index: 2)
                            }
                        case  3 : do {
                            self.pokeball(index: 3)
                            self.pokeball(index: 3)
                            }
                        default : print("Numero fuera de rango")
                        }
                        
                    } else {
                        mascotaCercana!.vida -= enemigo.ataque
                        //Sonido
                        playSound(fileName: "golpe")
                        //SystemSoundID.playFileNamed(fileName: "golpe", withExtenstion: "mp3")
                        //Cambiamos el tamaño de la imagen del enemigo segun se reduzga su vida
                        
                        mascotaCercana!.sizeIcon = mascotaCercana!.vida
                        
                        if (mascotaCercana!.sizeIcon > 50) {
                            mascotaCercana!.sizeIcon = 50
                        } else if (mascotaCercana!.sizeIcon < tamañoMinimoImagen) {
                            mascotaCercana!.sizeIcon = tamañoMinimoImagen
                        }
                        
                        mascotaCercana!.marcador.icon = mascotaCercana?.imagenBitmap?.withSize(width: CGFloat(mascotaCercana!.sizeIcon), height: CGFloat(mascotaCercana!.sizeIcon))
                        
                        //Si le quitamos vida al enemigo se alaja del personaje controlamos si es a la drechea o a la izquierda
                        if (mascotaCercana!.mirarIzquierda) {
                            enemigo.marcador.position =
                                CLLocationCoordinate2D(latitude: posicionInicial.latitude, longitude: posicionInicial.longitude - 0.0003)
                            mascotaCercana!.marcador.position = CLLocationCoordinate2D(
                                latitude: mascotaCercana!.marcador.position.latitude,
                                longitude: mascotaCercana!.marcador.position.longitude + 0.0003
                            )
                            
                        } else {
                            enemigo.marcador.position =
                                CLLocationCoordinate2D(latitude: posicionInicial.latitude, longitude: posicionInicial.longitude + 0.0003)
                            mascotaCercana!.marcador.position = CLLocationCoordinate2D(
                                latitude: mascotaCercana!.marcador.position.latitude,
                                longitude: mascotaCercana!.marcador.position.longitude - 0.0003
                            )
                            
                        }
                    }
                    
                    //Si le quitamos vida a la mascota se alaja del enemigo
                    // mascotaCercana.marcador.position = LatLng(posicionInicial.latitude - 0.0009, posicionInicial.longitude - 0.0009)
                    
                    
                }
            }
        }
        
        
        return coordenadasDestino(marcador: enemigo, posicionFinal: posicionFinal)
    }
    
    func moverPersonajes() {
        //Funcion asincrona que gestiona los movimientos de todos los elementos de juego en un bucle infinito
        if (!findeljuego){
            //Controlamos como se movera el personaje principal ------------------------------------------
            
            if (personaje!.vida <= 0) {
                if (edificiosArray.count != 0) {
                    irAposicion(coordenadasInicio: masCercanoDelArray(coordenadasInicio: personaje!.marcador.position, array: edificiosArray)!.marcador.position)
                    //personaje!!.marcador.isVisible = false
                    if (personaje!.imagenBitmap2 == nil) {
                        personaje!.imagenBitmap2 = personaje!.imagenBitmap
                        personaje!.imagenBitmap = UIImage(named: "ambulancia")
                        personaje!.marcador.icon = UIImage(named: "ambulancia")?.withSize(width: 50, height: 50)
                        
                    }
                    
                }
            } else {
                //Aqui es donde se controla como se mueve el personaje en cada ciclo
                if (seguirAMarcador != nil) {
                    // irAposicion(coordenadasInicio: seguirAMarcador!.position)
                    
                    personaje?.marcador.position = moverPersonaje(coordenadasInicio: (personaje?.marcador.position)!, posicionFinal: seguirAMarcador!.position, velocidad: personaje!.velocidad)
                }
                
            }
            
            if (irAposicion != nil){
                personaje?.marcador.position = moverPersonaje(coordenadasInicio: (personaje?.marcador.position)!, posicionFinal: irAposicion!.position, velocidad: personaje!.velocidad)
            }
            
            //Controlamos como se mueven las mascotas -------------------------------------------------
            if (mascotasArray.count != 0) {
                for mascota in mascotasArray {
                    if (mascota != nil) {
                        //Evaluamos lo lejos que estan los enemigos para perseguirlos o ir donde esta el personaje
                        let enemigoMasCercano = masCercanoDelArray(coordenadasInicio: mascota!.marcador.position, array: enemigosArray)
                        let distancia = distanciaA(posicion1: mascota!.marcador.position, posicion2: (personaje?.marcador.position)!)
                        
                        //La mascota sigue al personaje cuando el personaje pasa a menos de 50 metros de la mascota
                        if (distancia < 50){
                            if (!mascota!.seguirPersonaje){
                                mascota?.seguirPersonaje = true
                                mascota!.vida = 100
                                mascota!.marcador.zIndex = 1
                                mascota?.marcador.opacity = 1.0
                                //Sonido
                                playSound(fileName: "robot")
                                //SystemSoundID.playFileNamed(fileName: "robot", withExtenstion: "mp3")
                            }
                        }
                        
                        //Si esta activado seguir personaje compruebo si hay enemigos cerca para seguirlos
                        if (mascota!.seguirPersonaje){
                            if (enemigoMasCercano != nil){
                                if (distanciaA(posicion1: personaje!.marcador.position, posicion2: enemigoMasCercano!.marcador.position) < radioMascotaPerseguirEnemigo){
                                    
                                    //Evaluamos que elemneto del array de mascotas está mas cerca para atacar a los enemigos que no están siendo atacados
                                    var posicionElementoMasCercano: Marcador? = nil
                                    var distancia = 30000
                                    for elemento in mascotasArray {
                                        if ( elemento?.marcador.map != nil && elemento != mascota ){
                                            if (distanciaA(posicion1: (enemigoMasCercano?.marcador.position)!, posicion2: (elemento?.marcador.position)!) < distancia && elemento?.marcador.map != nil){
                                                posicionElementoMasCercano = elemento
                                                distancia = distanciaA(posicion1: (enemigoMasCercano?.marcador.position)!, posicion2: (elemento?.marcador.position)!)
                                            }
                                        }
                                    }
                                    //Controlamos que la mascota ataque al enemigo mas cercano que no tenga una mascota cerca
                                    if (posicionElementoMasCercano != nil) {
                                        if ( distanciaA(
                                            posicion1: enemigoMasCercano!.marcador.position,
                                            posicion2: posicionElementoMasCercano!.marcador.position
                                            ) < 100
                                            ) {
                                            var seguirAlMasCercano = true
                                            var seguirAEnemigo: Marcador? = nil
                                            for enemigo in enemigosArray {
                                                let mascotaMasCercana = masCercanoDelArray(coordenadasInicio: enemigo.marcador.position, array: mascotasArray )
                                                if (mascotaMasCercana != nil) {
                                                    if (distanciaA(
                                                        posicion1: enemigo.marcador.position,
                                                        posicion2: mascotaMasCercana!.marcador.position
                                                        ) > 100
                                                        ) {
                                                        
                                                        seguirAlMasCercano = false
                                                        seguirAEnemigo = enemigo
                                                        break
                                                    }
                                                    
                                                }
                                            }
                                            //Si todos los enemigos tiene una mascota cerca iremos donde el enemigo mas cercano
                                            if (seguirAlMasCercano) {
                                                mascota!.marcador.position = moverMascota(
                                                    mascota: mascota!, posicionFinal: enemigoMasCercano!.marcador.position,
                                                    velocidad: mascota!.velocidad
                                                )
                                            } else {
                                                mascota!.marcador.position = moverMascota(
                                                    mascota: mascota!, posicionFinal: seguirAEnemigo!.marcador.position,
                                                    velocidad: mascota!.velocidad
                                                )
                                            }
                                            
                                        } else {
                                            //Si el enemigo mas cercano no tine ninguna mascota atacandolo a menos de 100 metros movemos la mascota a este enemigo
                                            mascota!.marcador.position = moverMascota(
                                                mascota: mascota!, posicionFinal: enemigoMasCercano!.marcador.position,
                                                velocidad: mascota!.velocidad
                                            )
                                        }
                                    } else {
                                        //Si no hay mas mascotas atacamos al enemigo mas cercano
                                        mascota!.marcador.position = moverMascota(
                                            mascota: mascota!, posicionFinal: enemigoMasCercano!.marcador.position,
                                            velocidad: mascota!.velocidad
                                        )
                                    }
                                    
                                    //Si no hay enemigos cerca muevo la mascota hacia el personaje
                                } else {
                                    if (distancia > radioMascotaSeguirPersonaje) {
                                        mascota!.marcador.position = moverMascota( mascota: mascota!, posicionFinal: personaje!.marcador.position, velocidad: mascota!.velocidad )
                                    }
                                    
                                }
                            } else {
                                //Si no hay enemigos muevo la mascota a la posicion del personaje
                                if (distancia > radioMascotaSeguirPersonaje) {
                                    mascota!.marcador.position = moverMascota(mascota: mascota!, posicionFinal: personaje!.marcador.position, velocidad: mascota!.velocidad)
                                }
                                
                            }
                        }
                    }
                }
            }
            //Aqui es donde se controla como se mueven los enemigos en cada ciclo -------------------------------------
            if (enemigosArray.count != 0) {
                
                var eliminarEnemigo: Marcador? = nil
                for enemigo in enemigosArray {
                    
                    if (enemigo.vida <= 0) {
                        eliminarEnemigo = enemigo
                        enemigo.marcador.map = nil
                        
                    } else {
                        //Evaluo segun la velocidad si tengo que moverlo en este ciclo
                        //Evaluamos lo lejos que esta el personaje para perseguirlo o ir un edificio cercano
                        let edificioMasCercano = masCercanoDelArray(coordenadasInicio: enemigo.marcador.position, array: edificiosArray)
                        if (personaje!.marcador.map != nil && distanciaA(
                            posicion1: enemigo.marcador.position,
                            posicion2: personaje!.marcador.position
                            ) < distanciaEnemigoPersiguePersonaje
                            ) {
                            enemigo.marcador.position =
                                moverEnemigo(enemigo: enemigo, posicionFinal: personaje!.marcador.position, velocidad: enemigo.velocidad)
                        } else {
                            if (edificioMasCercano != nil) {
                                enemigo.marcador.position = moverEnemigo(
                                    enemigo: enemigo,
                                    posicionFinal: edificioMasCercano!.marcador.position,
                                    velocidad: enemigo.velocidad
                                )
                            }
                        }
                        
                    }
                }
                //Si la vida del enemigo es menor o igual a 0 lo eliminamos
                if (eliminarEnemigo != nil) {
                    /*
                     if (distanciaSaltos < 0.0005) {
                     distanciaSaltos = 0.00003
                     }
                     */
                    let energia = crearMarcador(coordenadasInicio: (eliminarEnemigo?.marcador.position)!, nombre: "Energia", imagen: UIImage(named: "atomo")!)
                    energia.imagenBitmap = UIImage(named: "atomo")
                    energia.puntuacion = eliminarEnemigo!.puntuacion
                    energia.sizeIcon = 25
                    energia.marcador.icon = UIImage(named: "atomo")?.withSize(width: 25, height: 25)
                    
                    energiaArray.append(energia)
                    /*
                     for (ruinas in ruinasArray) {
                     ruinas!!.marcador.remove()
                     }
                     */
                    //ruinasArray.removeAll(ruinasArray)
                    
                    personaje!.puntuacion += eliminarEnemigo!.puntuacion
                    //Toast.makeText(applicationContext, "${eliminarEnemigo.puntuacion} Puntos Más", Toast.LENGTH_SHORT).show()
                    //puntuacionTextView.text = personaje!!.puntuacion.toString()
                    puntuacionLabel.text = String(personaje!.puntuacion)
                    ejecutarRespawn(respawn: eliminarEnemigo)
                    
                    enemigosArray.removeObjet(object: eliminarEnemigo!)
                    
                    //Sonido
                    playSound(fileName: "muertemostruo")
                    //SystemSoundID.playFileNamed(fileName: "muertemonstruo", withExtenstion: "mp3")
                    
                    seguirAMarcador = masCercanoDelArray(coordenadasInicio: personaje!.marcador.position, array: enemigosArray)?.marcador
                    
                    
                }
            }
            //Aqui es donde se controla el movimiento de la energia
            if (energiaArray.count != 0) {
                var eliminarEnergia: Marcador? = nil
                for energia in energiaArray {
                    
                    energia.marcador.position = coordenadasDestino(marcador: energia, posicionFinal: referenciaParaRefresco!)
                    
                    if (distanciaA(posicion1: energia.marcador.position, posicion2: referenciaParaRefresco!) <= 10) {
                        eliminarEnergia = energia
                    }
                }
                //Si eliminamos el marcador energia
                if (eliminarEnergia != nil) {
                    eliminarEnergia!.marcador.map = nil
                    energiaArray.removeObjet(object: eliminarEnergia!)
                    
                    let size = Int(circle.radius / 50) + 50
                    edificiosArray[0].marcador.icon = UIImage(named: "casa")?.withSize(width: CGFloat(size), height: CGFloat(size))
                    
                    circle.radius += Double(eliminarEnergia!.puntuacion)
                    if (circle.radius > 2000) {
                        finDelJuego(isWiner: true, mensaje: "Ganaste la ciudad esta protegida Puntuación actual: \(personaje!.puntuacion) ")
                    }
                    //Sonido
                    playSound(fileName: "objeto")
                   //SystemSoundID.playFileNamed(fileName: "objeto", withExtenstion: "mp3")
                    
                }
            }
            
            //Aqui es donde se controla el movimiento del paquete de objeto
            //Aqui es donde se controla cuando se eliminan los objetos
            
            if (objetosArray.count != 0) {
                
                for objeto in objetosArray {
                    
                    if ( objeto.marcador.map != nil) {
                        if (objeto.irAposicion.latitude != 0.0) {
                            
                            objeto.marcador.position = coordenadasDestino(marcador: objeto, posicionFinal: objeto.irAposicion)
                            
                            if (distanciaA(posicion1: objeto.marcador.position, posicion2: objeto.irAposicion) <= 10) {
                                objeto.irAposicion = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
                                objeto.marcador.icon = objeto.imagenBitmap?.withSize(width: 25, height: 25)
                            }
                        }
                        
                        
                    }
                    
                }
                
            }
            
            //Aqui es donde se controla cuando se eliminan los edificios
            if (edificiosArray.count != 0) {
                var elimiarEdificio: Marcador? = nil
                var edificiosVisibles = false
                for edificio in edificiosArray {
                    if (edificio.marcador.map != nil) {
                        edificiosVisibles = true
                    }
                    if (edificio.vida <= 0) {
                        
                        //edificio.marcador.title = "ruinas"
                        if (edificiosArray.count == 1) {
                            // Toast.makeText(applicationContext, "Todos los edificios fueron destruidos", Toast.LENGTH_LONG).show()
                            finDelJuego(isWiner: false, mensaje: "Todos los edificios han sido destruidos")
                        } else {
                            elimiarEdificio = edificio
                        }
                        
                    }
                }
                // Si todos los edificios han sido destruidos comprobamos si en el array centrosArray quedan centros
                if (!edificiosVisibles) {
                    // Si quedan centros en centrosArray activo el ultiomo del array pera que pueda ser atacado
                    if (centrosArray.count > 0) {
                        edificiosArray[0] = crearMarcador(coordenadasInicio: centrosArray.last!.marcador.position, nombre: centrosArray.last!.nombre, imagen: UIImage(named: "casa")!)
                        referenciaParaRefresco = edificiosArray[0].marcador.position
                        circle.position = referenciaParaRefresco!
                        circle.radius = 1000.0
                        personaje!.marcador.position = referenciaParaRefresco!
                        circleAtaque.position = referenciaParaRefresco!
                        centrosArray.last!.marcador.map = nil
                        centrosArray.removeObjet(object: centrosArray.last!)
                        if (polilineasArray.count > 0) {
                            polilineasArray.last!.map = nil
                            polilineasArray.removeObjet(object: polilineasArray.last!)
                        }
                        mapView.camera = GMSCameraPosition.camera(withTarget: referenciaParaRefresco!, zoom: 13.0)
                        self.showAlertView(title: "La ciudad \(edificiosArray[0].nombre) ha sido destruida", message: nil)
                        
                    //Si no quedan centros en arrayCentros entonces el juego termina
                    } else {
                        finDelJuego(isWiner: false, mensaje: "La ultima cidudad \(edificiosArray[0].nombre) ha sido destruida")
                        findeljuego = true
                    }
                    
                    
                
                }
                
                //Si la vida del edificio es menor o igual a 0 lo eliminamos
                if (elimiarEdificio != nil) {
                    ruinasArray.append(crearMarcador(coordenadasInicio: elimiarEdificio!.marcador.position, nombre: "Ruina", imagen: UIImage(named: "ruina")!))
                    elimiarEdificio!.vida = 100
                    elimiarEdificio!.marcador.map = nil
                    //edificios.remove(elimiarEdificio)
                    //Sonido
                    playSound(fileName: "explosion")
                    //SystemSoundID.playFileNamed(fileName: "explosion", withExtenstion: "mp3")
                }
            }
            
            //Llamamos de forma recursiva a la funcion moverPersonajes
            DispatchQueue.main.async {
                self.ciclos += 1
                if (self.ciclos >= 100) {
                    self.ciclos = 0
                }
                self.moverPersonajes()
            }
        }
    }
    
    
    //Evaluamos en que direccion y cuanto se va a mover y lo movemos a las nuevas coordenadas
    func coordenadasDestino(marcador: Marcador, posicionFinal: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        var posicion: CLLocationCoordinate2D
        let posicionInicial = marcador.marcador.position
        
        
        if (posicionInicial.longitude < posicionFinal.longitude) {
            //Si el marcador se despalza a la derecha pongo su icono mirando a la derecha
            
            if (marcador.mirarIzquierda) {
                if (marcador.cambiarPosicionImagen) {
                    if (marcador.imagenBitmap != nil) {
                        marcador.marcador.icon = marcador.imagenBitmap?.withSize(width: CGFloat(marcador.sizeIcon), height: CGFloat(marcador.sizeIcon))
                    }
                    marcador.mirarIzquierda = false
                    marcador.cambiarPosicionImagen = false
                } else {
                    marcador.cambiarPosicionImagen = true
                }
            }
            posicion = CLLocationCoordinate2D(latitude: posicionInicial.latitude, longitude: posicionInicial.longitude + self.distanciaSaltos)
        } else {
            //Si el marcador se despalza a la izquierda pongo su icono mirando a la izquierda
            if (!marcador.mirarIzquierda) {
                if (marcador.imagenBitmap != nil) {
                    
                    let imagenFinal = marcador.imagenBitmap?.withSize(width: CGFloat(marcador.sizeIcon), height: CGFloat(marcador.sizeIcon))
                    //let flippedImage = imangeFinal?.withHorizontallyFlippedOrientation()
                    
                    marcador.marcador.icon = imagenFinal?.flipImageLeftRight()
                    marcador.mirarIzquierda = true
                    marcador.cambiarPosicionImagen = false
                }
                
            }
            marcador.cambiarPosicionImagen = false
            posicion = CLLocationCoordinate2D(latitude: posicionInicial.latitude, longitude: posicionInicial.longitude - self.distanciaSaltos)
            
        }
        
        if (posicionInicial.latitude < posicionFinal.latitude) {
            posicion = CLLocationCoordinate2D(latitude: posicion.latitude  + self.distanciaSaltos, longitude: posicion.longitude)
            
        } else {
            posicion = CLLocationCoordinate2D(latitude: posicion.latitude  - self.distanciaSaltos, longitude: posicion.longitude)
            
        }
        
        return posicion
    }
    
    
    func crearMarcador(coordenadasInicio: CLLocationCoordinate2D, nombre: String, imagen: UIImage) -> Marcador {
        let marker = GMSMarker(position: coordenadasInicio)
        
        marker.icon = imagen.withSize(width: 50, height: 50)
        marker.map = mapView
        let marcador = Marcador(marcador: marker)
        marcador.nombre = nombre
        return marcador
        
    }
    
    func moverPorCiclo(velocidad : Int) -> Bool {
        let num1 = (100 / velocidad)
        var mover = false
        var suma = num1
        while (suma <= 100) {
            if (suma == ciclos) {
                mover = true
            }
            suma += num1
        }
        return mover
        
    }
    
    //Moastrar aletar
    func showAlertView(title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        //alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            // your code here
            //print("Bye. Lovvy")
            alertController.dismiss(animated: true, completion: nil)
        }
    }
    
    func finDelJuego(isWiner: Bool , mensaje: String) {
        if (isWiner){
            self.showAlertView(title: "\(mensaje)", message: nil)
            
            //Consultamos la puntuacion del ranquing para el mundo en el que estamos
            //Actualizamos el ranking
            
            let queryMundos = PFQuery(className:"Mundos")
            queryMundos.whereKey("nivel", equalTo: self.idMundo?["nivel"] as? String)
            queryMundos.findObjectsInBackground { (objects, error) in
                if error == nil {
                    // The find succeeded.
                    // Do something with the found object
                    for object in objects ?? [] {
                        //print("\(object["nombre"])")
                        let puntuacionRanking = object["puntuacionRanking"] as! Int
                        if (puntuacionRanking < self.personaje!.puntuacion){
                            object["puntuacionRanking"] = self.personaje?.puntuacion
                            object["usuarioRanking"] = self.personaje?.nombre
                            
                            // Saves the new object.
                            object.saveInBackground {
                                (success: Bool, error: Error?) in
                                if (success) {
                                    // The object has been saved.
                                } else {
                                    // There was a problem, check error.description
                                }
                            }
                            self.idMundosUsuarios!["puntuacion"] = self.personaje?.puntuacion
                            self.idMundosUsuarios!.saveInBackground {
                                (success: Bool, error: Error?) in
                                if (success) {
                                    // The object has been saved.
                                } else {
                                        // There was a problem, check error.description
                                }
                                
                            }
                        
                        }
                    }
                    
                } else {
                    // Log details of the failure
                    if let error = error, let user = (error as NSError?)?.userInfo {
                        print("Error: \(error) \(user)")
                    }
                }
            }
            
            
            oleada += 1
            if (distanciaSaltos < 0.00009){
                distanciaSaltos += 0.00001
            }
            let bunker = crearMarcador(coordenadasInicio: referenciaParaRefresco!, nombre: "Protegida", imagen: (UIImage(named: "casa")?.withSize(width: 50, height: 50))!)
            var siguientePosicion = posicionAleatoriaRelativaEnemigos(coordenadasInicio: referenciaParaRefresco!)
            
            if(ubicacionesArray.count > oleada + 1 && distanciaA(posicion1: referenciaParaRefresco!, posicion2: ubicacionesArray[oleada].placemark.coordinate) < 10000){
                siguientePosicion = ubicacionesArray[oleada].placemark.coordinate
            }
            
            
            
            let path = GMSMutablePath()
            path.add(referenciaParaRefresco!)
            path.add(siguientePosicion)
            let polilinea = GMSPolyline(path: path)
            polilinea.strokeWidth = 5.0
            polilinea.geodesic = true
            polilinea.strokeColor = .yellow
            polilinea.map = mapView
            
            polilineasArray.append(polilinea)
            
        
            referenciaParaRefresco = siguientePosicion
            centrosArray.append(bunker)
            
            for (i, edificio) in edificiosArray.enumerated() {
                if (i == 0) {
                    edificio.marcador.icon = UIImage(named: "casa")!.withSize(width: 50, height: 50)
                    edificio.marcador.position = referenciaParaRefresco!
                    edificio.vida = 100
                    //edificio.nombre = ubicacionesArray[oleada]?.nombre!
                    
                } else {
                    edificio.marcador.position = posicionAleatoriaRelativaEdificios(coordenadasInicio: referenciaParaRefresco!)
                    edificio.marcador.map = mapView
                    edificio.vida = 100
                }
            }
            
            circle.radius = 100.0
            circle.position = referenciaParaRefresco!
            circle.map = mapView
            personaje!.marcador.position = referenciaParaRefresco!
            irAposicion(coordenadasInicio: referenciaParaRefresco!)
            circleAtaque.position = personaje!.marcador.position
            
            
            for ruinas in ruinasArray {
                ruinas.marcador.map = nil
            }
            ruinasArray.removeAll()
            
            if (personaje!.distanciaAtaque < 100) {
            
                for objeto in objetosArray {
                
                    objeto.marcador.position = posicionAleatoriaRelativaEdificios(coordenadasInicio: referenciaParaRefresco!)
                    objeto.marcador.map = mapView
                
                    objeto.marcador.icon = UIImage(named: "paquete")?.withSize(width: 50, height: 50)
                    objeto.irAposicion = CLLocationCoordinate2D(latitude: objeto.marcador.position.latitude - 0.01, longitude: objeto.marcador.position.longitude + 0.01)
                
                }
            }
            //Creo las mascotas o amigos que ayudaran al personaje
            if (oleada < 5) {
                let velocidad = 50
                let arrayImagenes: Array<UIImage> = [UIImage(named: "tank")!, UIImage(named: "eva")!, UIImage(named: "robotmanos")!]
                let i = oleada
                let marcador = crearMarcador( coordenadasInicio: posicionAleatoriaRelativaEdificios(coordenadasInicio: referenciaParaRefresco!), nombre: "robot", imagen: arrayImagenes[i - 2]
                )
                //marcador.marcador.zIndex = 3000f
                marcador.velocidad = velocidad
                marcador.marcador.map = mapView
                //marcador.seguirPersonaje = true
                marcador.marcador.opacity = 0.5
                marcador.imagenBitmap = arrayImagenes[i - 2]
                mascotasArray[i - 1] = marcador
                
            }
            
            for mascota in mascotasArray {
                if (mascota != nil) {
                    mascota!.marcador.position = posicionAleatoriaRelativaEdificios(coordenadasInicio: referenciaParaRefresco!)
                }
            }
            for energia in energiaArray {
                
                energia.marcador.position = posicionAleatoriaRelativaEdificios(coordenadasInicio: referenciaParaRefresco!)
                
            }
            
            for enemigo in enemigosArray {
                enemigo.marcador.map = nil
            }
            enemigosArray.removeAll()
            
            for respawn in respawnArray {
                respawn.marcador.map = nil
                //respawn!!.marcador.position = posicionAleatoriaRelativaEnemigo(referenciaParaRefresco!!)
            }
            
            for (count, respawn) in respawnArray.enumerated() {
                if (count < oleada + 1 && count <= 4) {
                    ejecutarRespawn(respawn: respawn)
                }
            }
            mapView.camera = GMSCameraPosition.camera(withTarget: (referenciaParaRefresco)!, zoom: 13.0)
            seguirAMarcador = masCercanoDelArray(coordenadasInicio: personaje!.marcador.position, array: enemigosArray)?.marcador
        } else {
            let alertController = UIAlertController(title: title, message: mensaje, preferredStyle: .alert)
            
            let reintentar = UIAlertAction(title: "Reintentar", style: .default) { (alertAction) in
                //self.view.layoutIfNeeded()
               // self.viewDidLoad()
                //self.viewWillAppear(true)
                self.reiniciar()
                
            }
            alertController.addAction(reintentar)
            let salir = UIAlertAction(title: "Salir", style: .cancel) { (alertAction) in
               self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(salir)
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    /*
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func reiniciar(){
    personaje = nil
    
    referenciaParaRefresco = nil
    irAposicion = nil
    seguirAMarcador = nil
    
    
    edificiosArray = []
    enemigosArray = []
    mascotasArray = [nil,nil,nil,nil]
    pokeballArray = [nil,nil,nil,nil]
    objetosArray = []
    respawnArray = []
    ruinasArray = []
    energiaArray = []
    centrosArray = []
    polilineasArray = []
    ubicacionesArray = []
    
    
    primeraVez = true
    findeljuego = false
    oleada = 1
    ciclos = 0
    
    reloadViewFromNib()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        findeljuego = true
    }
    
    //MÉTODO QUE REPRODUCE UN SONIDO con el voluemen actual contrloado por el usuario
    
    func playSound(fileName: String) {
        if let audioPlayer = audioPlayer, audioPlayer.isPlaying { audioPlayer.stop() }
        
        guard let soundURL = Bundle.main.url(forResource: fileName, withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
            try AVAudioSession.sharedInstance().setActive(true)
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
            audioPlayer?.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
extension UIViewController {
    func reloadViewFromNib() {
        let parent = view.superview
        view.removeFromSuperview()
        view = nil
        parent?.addSubview(view) // This line causes the view to be reloaded
    }
}
///MÉTODO QUE REPRODUCE UN SONIDO DE MENOS DE 30 SEGUNDOS con el volumen del sistema para alertas
extension SystemSoundID {
    static func playFileNamed(fileName: String, withExtenstion fileExtension: String) {
        var sound: SystemSoundID = 0
        if let soundURL = Bundle.main.url(forResource: fileName, withExtension: fileExtension) {
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &sound)
            AudioServicesPlaySystemSound(sound)
        }
    }
}

extension UIImage {
    
    func withSize(width : CGFloat , height : CGFloat) -> UIImage? {
        // size has to be integer, otherwise it could get white lines
        let size = CGSize(width: floor(width), height: floor(height))
        UIGraphicsBeginImageContext(size)
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func flipImageLeftRight() -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: self.size.width, y: self.size.height)
        context.scaleBy(x: -self.scale, y: -self.scale)
        
        context.draw(self.cgImage!, in: CGRect(origin:CGPoint.zero, size: self.size))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    
}

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func removeObjet(object: Element) {
        guard let index = firstIndex(of: object) else {return}
        remove(at: index)
    }
    
}
